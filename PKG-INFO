Metadata-Version: 1.1
Name: file_encryptor
Version: 0.2.9
Summary: Tool for convergently encrypting files used by MetaDisk.
Home-page: https://github.com/Storj/file-encryptor
Author: Storj Labs
Author-email: hello@storj.io
License: The MIT License (MIT)

Copyright (c) 2014 Storj Labs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
Download-URL: https://github.com/storj/file-encryptor/tarball/0.2.9
Description: File Encryptor
        ==============
        
        |Build Status| |Coverage Status| |PyPI version|
        
        This is a library used by MetaDisk to convergently encrypt and decrypt
        files. It contains helper methods to encrypt and decrypt files inline
        (without using extra space) and to stream decryption.
        
        Installation
        ------------
        
        You can easily install ``file-encryptor`` using pip:
        
        ::
        
            pip install file_encryptor
        
        Usage
        -----
        
        Here’s an example to encrypt a file inline using convergent encryption:
        
        .. code:: python
        
            import file_encryptor.convergence
        
            key = convergence.encrypt_inline_file("/path/to/file", None)
        
        You can also specify a passphrase:
        
        .. code:: python
        
            import file_encryptor.convergence
        
            key = convergence.encrypt_inline_file("/path/to/file", "rainbow dinosaur secret")
        
        To decrypt a file inline, you need the key that was returned by the
        encrypt method:
        
        .. code:: python
        
            import file_encryptor.convergence
        
            key = convergence.encrypt_inline_file("/path/to/file", "rainbow dinosaur secret")
        
            convergence.decrypt_inline_file("/path/to/file", key)
        
        The reason why you cannot use the passphrase directly is because the key
        is derived from both the passphrase and the SHA-256 of the original
        file.
        
        For streaming applications, you can decrypt a file with a generator:
        
        .. code:: python
        
            for chunk in convergence.decrypt_generator("/path/to/file", key):
                do_something_with_chunk(chunk)
        
        Cryptoconcerns
        --------------
        
        The key generation mechanism is the following:
        
        .. code:: python
        
            key = HMAC-SHA256(passphrase, hex(SHA256(file-contents)))
        
        If no passphrase is given, a default is used.
        
        The file itself is encrypted using AES128-CTR, from pycrypto. We’re not
        specifying any IV, thinking that for convergent encryption that is the
        right thing to do.
        
        Testing
        -------
        
        To run tests, execute the following command in the project root:
        
        ::
        
            python setup.py test -a "--doctest-modules --pep8 -v tests/"
        
        To run tests with detailed coverage output, execute:
        
        ::
        
            coverage run setup.py test -a "--doctest-modules --pep8 -v tests/"
            coverage report -m --include="file_encryptor/*"
        
        .. |Build Status| image:: https://travis-ci.org/Storj/file-encryptor.svg
           :target: https://travis-ci.org/Storj/file-encryptor
        .. |Coverage Status| image:: https://coveralls.io/repos/Storj/file-encryptor/badge.png?branch=master
           :target: https://coveralls.io/r/Storj/file-encryptor?branch=master
        .. |PyPI version| image:: https://badge.fury.io/py/file_encryptor.svg
           :target: http://badge.fury.io/py/file_encryptor
Keywords: storj,metadisk,convergent encryption
Platform: UNKNOWN
